package PT2017.Assignment5.HW5;

import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
public class ProcessData {
	
private List<MonitoredData> data;    //list of read data
private FileWriter writer;           //the text file in which the resulted filtered data will be written
public ProcessData(){
	try {
		writer=new FileWriter("activity_occurences.txt");
	} catch (IOException e) {
		
		e.printStackTrace();
	}
	data=new ArrayList<MonitoredData>();
}

public void readData(){   //reads data from input text file and saves it in the data variable
	
	String line,activity="";
	DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	Date date1=null, date2=null;
	try {
		FileReader file=new FileReader("D:/PT2017_30424_CopaciuDalia_Assignment_5/Activities.txt");
		BufferedReader reader= new BufferedReader(file);
		line = reader.readLine();
		while(line!=null){
			String[] fileData = line.split("\t\t");    //remove the tabs between data on the same line
			try {
				date1 = format.parse(fileData[0]);     // start time
				date2=format.parse(fileData[1]);       //end time
			} catch (ParseException e) {
				e.printStackTrace();
			}
			activity=fileData[2];        //activity name as string 
			MonitoredData mnew=new MonitoredData(date1, date2,Activity.valueOf(activity.trim()));
			data.add(mnew);
			line=reader.readLine();     //read a new line
		} 
		reader.close();
	} catch (IOException e) {
		e.printStackTrace();
	}
}
public int countDistinctDays(){    //counts the distinct days that appear in the input data file
	return (int) data.stream()
			.map(MonitoredData::getDay)    //maps the days (integers)
			.distinct()                   //gets only the distinct elements
			.count();                    //counts the distinct days
}
public Map<String,Long> writeActivitiyOccurences(boolean write){  //gets for each activity its total nr of occurrences
	
	Map<String,Long> map = new HashMap<String, Long>();
	map=data.stream()    
			.collect(Collectors.groupingBy(MonitoredData::getActivity,Collectors.counting()));
	if(write)
		try {
			writer.write("Exercise 2 \r\n\r\n");
			for(String key:map.keySet()){
				writer.write(key+" appears "+map.get(key)+" times\r\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	return map;
}
public void writeDailyActivities(){   //gets for each day the present activities with their nr of occurrences in that day
	 Map<Integer, Map<String, Long>> map= new HashMap<Integer, Map<String, Long>>();
	 map=data.stream()   
			 .collect(Collectors.groupingBy(MonitoredData::getDay, Collectors.groupingBy(MonitoredData::getActivity,Collectors.counting())));
	try {
		writer.write("\r\n\r\nExercise 3 \r\n\r\n");
		for(int key:map.keySet()){
			Map<String, Long>m=map.get(key);
			writer.write("Day "+key+"\r\n");
			for(String act:m.keySet())
				writer.write(act+" appears "+m.get(act)+" times\r\n");
		}
	} catch (IOException e) {
		e.printStackTrace();
	}
}
public int[] transform(int min){   //transforms from minutes to hours and minutes
	int pal[] = new int[2],hour=0,m=min;
	while(m>59){
		hour++;
		m-=60;
	}
	pal[0]=hour;
	pal[1]=m;
	return pal;
}

public void larger10Hours(){   //gets the activities that have a total duration larger than 10 hours
	
	Map<String, Integer> mapp= new HashMap<String, Integer>();
	List<String> map2= new ArrayList<String>();
	mapp=data.stream()    //gets for each activity the total duration 
			.collect(Collectors.groupingBy(MonitoredData::getActivity, Collectors.summingInt(MonitoredData::getDuration)));
	map2= mapp.entrySet().stream()    //gets only those activities that have the duration > 10 h
			.filter(p->p.getValue()>600)   //600 mins = 10 h 
			.map(p->p.getKey())           //maps the activity names
			.collect(Collectors.toList());
	
	try {
		writer.write("\r\n\r\nExercise 4 (Activities that take longer than 10 hours) \r\n\r\n");
		for(String act:map2){
			int hour=transform(mapp.get(act))[0];    //transformation from minutes to hours and minutes
			int min=transform(mapp.get(act))[1];
			writer.write(act+" takes "+hour+" hours and "+min+" minutes."+"\r\n");
		}
	} catch (IOException e) {
		e.printStackTrace();
	}
}

public void lessThan5Mins(){  //gets the activities that has in 90% of cases a duration less than 5 minutes 
     Map<String,Long> occur=writeActivitiyOccurences(false);   //gets the activities and for each activity the the total nr of occurrences
	 List<String>lst=new ArrayList<String>();
	 Map<String,Long> howmany=new HashMap<String,Long>();
	 howmany=data.stream()     //gets the activities and for each activity the nr of occurrences for which the duration is less than 5 minutes
			 .filter(p->p.getDuration()<5)   //gets only the elements that have a duration < 5 minutes
			 .collect(Collectors.groupingBy(MonitoredData::getActivity,Collectors.counting()));   //counts them
	 lst=howmany.entrySet().stream()   //checks the 90% of cases condition
			 .filter(p->p.getValue()>=(float)90/100*occur.get(p.getKey()))
			 .map(p->p.getKey())
			 .collect(Collectors.toList());
	 
	 try {
			writer.write("\r\n\r\nExercise 5 (90% of the monitoring samples with duration less than 5 minutes) \r\n\r\n");
			for(String act:lst){
				writer.write(act+" takes less than 5 minutes in 90% of times"+"\r\n");
			}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
}

}
