package PT2017.Assignment5.HW5;
import java.util.*;
public class MonitoredData {
	
private Date startTime;
private Date endTime;
private Activity activityLabel;

public MonitoredData(Date start, Date end, Activity activity){
	startTime=start;
	endTime=end;
    activityLabel=activity;
}

public int getDay(){
	Calendar c=Calendar.getInstance();
	c.setTime(startTime);
	return c.get(Calendar.DAY_OF_MONTH);
}
public String getActivity(){
	return activityLabel.toString();
}

public int getDuration(){
	long diff = endTime.getTime()-startTime.getTime();
	long sec = diff / 1000 % 60;
	long min = diff / (60 * 1000) % 60;
	long hour = diff / (60 * 60 * 1000) % 24;
	while(sec>59){
		min++;
		sec-=60;
	}
	while(hour>0){
		hour--;
		min+=60;
	}
	return (int)min;
}
}
